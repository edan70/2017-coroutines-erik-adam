# About this Compiler Extension
This project was originally motivated by the resulting complexity of
simulation software, even for simple systems when expressed in standard
Java code. The goal was to produce a compiler extension that provides
similar functionality for coroutines as that found in the simulation
package available in Simula 67. Coroutines are a convenient tool,
which as we will see, greatly simplifies the way in which we can write
simulation code. This extension introduces syntactic sugar which
allows the user to write coroutines in what we call *Javasim* and
translate such code into equivalent standard Java.

Javasim is implemented as transformations on valid standard Java code,
meaning that Javasim looks exactly like standard Java but the Javasim
transformations is carried out under conditions described further into this
manual. 

In an attempt to avoid a too technical description of coroutines
we will try to introduce the concept by example. This is not an
introduction to simulations in any way, but our goal is that
you should be able to understand how to use this extension after
reading this manual and to inspire you to find applications for
coroutines in your own applications and projects.

## Introducing Coroutines by Example

### Coroutines in Simulations

We would like to express our simulation as a set of interacting
processes executing in parallel in a non-preemptive environment,
using a deterministic scheduler.

Creating a separate thread for each simulated process is certainly
overkill. Instead, what we want are *coroutines*. Coroutines are
sometimes described as light-weight processes or as a generalized 
subroutines. They provide process like behaviour in the sense that
they are executable and schedulable entities allowed to run until they
return control to the caller (non-preemptive). They are also much like
subroutines with multiple entry points able to maintain state between
activations. The closest thing to a subroutine we find in Java is a class
method.

What this extension provide is a way for you to write coroutines
simply by implementing a specific interface which we call Coroutine.
This interface has two methods ``doActivate`` and ``detach``. These
will be described as we go along, but keep an eye out for them in
code excerpts.

Throughout the remainder of this manual, process and coroutine are
used synonymously.

### The Simulation Loop

The first thing we need is a simulation loop which make time
progress in the simulated environment. This loop is responsible
for scheduling and activating coroutines according to a
deterministic schedule. The loop looks something like this:

```java
while (time < simulationTime) {
	Event e = queue.poll();
	time = e.getT();
	e.getProcess().doActivate();
}
```

The simulation loop simply takes the next scheduled process and
activates it by calling ``doActivate``. Note that there is only
a single thread running the simulation loop, which enters the
next coroutine in its current state through ``doActivate``.
The coroutine is allowed to run until it returns after
which the next coroutine can be activated.

Processes can interact with each other, one process can reschedule
itself or another process by creating an event in the ``queue``.
A process can also put itself or another process in a wait queue
from which a process cannot reenter the ready queue (and later
be activated again) unless it is reactivated by another process.

### Hospital Simulation

Consider a simple hospital environment consisting of doctors,
patients, a wait room, and a coffe� room. Patients arrive at the
hospital according to some distribution and notify staff of
their arrival; while waiting for their turn they sit down in the
wait room. At the hospital there is a team of doctors ready to
aid the waiting patients. Doctors (being the limited resource in
this simulation) has a simplified work procedure that goes something
like this:

    If the wait room is empty, wait in the coffe� room for the next
    patient to arrive, otherwise, estimate the treatment time for
    the next patient and give aid (removing the patient from the wait room).

As part of the simulation we introduce two processes: ``Hospital`` and
``Doctor``. The Hospital process takes on the task of generating patients
and places them in the wait room. Its procedure is as follows:

    Create an instance of the ``Patient`` class representing an
    arriving patient and place the patient in the wait room.
    Schedual the arrival of the next patient at some future time
    in the simulation i.e., reschedual the activation of Hospital
    at that time to generate the next patient.

Patients themselves are passive processes in this simulation so they will
simply be passed around as instances of a ``Patient`` class.

Before we implement the named classes above, lets add a base class for our
processes:

```java
public abstract class Process implements Coroutine {
	protected Simulation s;
	public Process(Simulation s) { this.s = s; }

	public void hold(double time) {
	    Event e = new Event(this, time);
	    s.queue(e);
	    detach();
	}

	public void wait() {
	    detach();
	}

	public abstract void doActivate();
}
```

``Process`` implements ``Coroutine`` previously mentioned
which enables the features of this extension. Note the calls
to ``detach`` in methods ``hold`` and ``wait``. Detach allows
coroutines to directly yield control back to the caller of
``doActivate``, pausing execution of the routine at the
location where ``detach`` is called. The process will progress
no further unless activated again, that is, until someone
calls its ``doActivate`` method.

Another important part of this base class is the
field ``protected Simulation s`` holding a reference to an
instance of class Simulation. This enables processes to
interact with the running simulation and its entities using
events. A process extending this base class can for example
reschedual itself to be activated at a later time using the
inherited method ``hold``. Since ``hold`` ends with a call
to ``detach``, the running processes scheduals itself to be
activated at the specified time and then returns control back
to the simulation loop which called its ``doActivate``.
The process will effectively hold its execution until the
simulation reactivates the process.

Lets see how we can implement the ``Hospital`` and ``Doctor``
processes in Javasim as extensions of base class ``Process``:

```java
public class Hospital extends Process {

	public static final double timeBetweenArrivals = 5;
    public static final double treatmentTime = 6;
	 
    private List<Patient> waitRoom   = new LinkedList<Patient>();
	private List<Doctor>  coffeeRoom = new LinkedList<Doctor>();
	 
	public Hospital(Simulation s) {
		super(s);
	}

	private Patient patientArrives() {
		System.out.println("A patient arrives.");
	    waitRoom.add(new Patient());
	 }

	 private void holdUntilNextPatientArrives() {
	 	 hold(timeBetweenArrivals);
	 }

	 private void callForDoctor() {
	     if (coffeeRoom.size() > 0) {
	         activateNow(coffeRoom.pollFirst());
	     }	         
	 }

     public void doActivate() {
	     patientArrives();
	     callForDoctor();
	     holdUntilNextPatientArrives();
	 }

	 public Patient nextPatient() {
	     return waitRoom.pollFirst();
     }

	 public void addToCoffeeRoom(Doctor doctor) {
	     coffeeRoom.addLast(doctor);
	 }
}
```

```java
public class Doctor extends Process {
    private Hospital h;
    private Patient p;
	 
	public Doctor(Hospital h, Simulation s) {
	    super(s);
	    this.h = h;
	}

	private boolean waitRoomIsEmpty() {
	    return h.waitRoomEmpty();
	}

	private void waitInCoffeeRoom() {
	    h.addToCoffeeRoom(this);
	    wait();
	}

	private void treatNextPatient() {
	    Patient p = h.nextPatient()
	    System.out.println("Treating Patient: " + p);
	    hold(Hospital.treatmentTime);
	    System.out.println("Treatment done: " + p);
	}

	public void doActivate() {
	    if (waitRoomIsEmpty()) {
	        waitInCoffeeRoom();
	    } else {
	    	treatNextPatient();
	    }
	}
}
```

As we can see, ``doActivate`` in classes ``Hospital`` and ``Doctor``
implements the procedures described earlier almost exactly as when
written in plain English.

The point we want to make with this example is that by
providing the ``doActivate`` and ``detach`` methods, we can
provide the neccessary control flow transformations behind the
scenes in the compiler, so that you can express the behaviour
of your processes without mixing it with awkward control flow
statements that pass execution control from process to process.

The ``doActivate`` method is used to activate a process. That is,
to resume its execution in its current state. The ``detach`` method
is used to return control from ``doActivate`` to the caller of
``doActivate``.

## Limitations and Implementation Details

The compiler (by design) considers the ``doActivate`` method as the
entry point for a coroutine. In order to be able to use a single
Java method as the entry point for a process we must for technical
reasons inline all method calls which somewhere in the corresponding
call chain makes a call to detach. Effectively all calls to ``detach``
occurs inside ``doActivate``. Using this technique, we limit our
implementation to non-recusive coroutines since inlining a recursive
method is not possible; the body would be expanded recursively
inside itself an infinite number of times.

Though we have tried to generalize the use of detach as much as
time allowed, free use of detach brings several problematic
cases concerning state management. For example,
allowing the user to call a method on an arbitrary class instance
that contains a call to ``detach`` would force all fields accessed
in that methods enclosing class to be ``public`` to be accessible
by the inlined code in ``doActivate``, which is not desirable.
Also, from a perspective of design, if the called method
affects the life cycle of the process in an intimate way it should
probably be part of the process and not forced upon the process from
the outside. Such designs most likely lead to a brittle code base
since the behaviour of processes can be spread out through multiple
classes and be used by several processes. Changing one such method
could have severe impact on the set of processes utilizing the same method.

## Using the compiler

First off the compiler has to be built, follow the instructions in the [readme](README.md)
to build the compiler. The compiler should now exist as the file: ``coroutines.jar`` 
in the ``coroutines`` directory. Now, to translate a javasim file called
``Example.javasim`` Simply pass it as an argument to the jar file and redirect the output accordingly:

	java -jar coroutines.jar Example.javasim > Example.java
	
The generated java file may now be treated as any other ordinary java file.
Consider that a ``Coroutine`` interface has to be visible to the jar file.
We provide you with a ``coroutine`` package in the same directory as the 
jar file that contains such an interface that may be used.

## Running the Hospital example

Here is how to build and run the enclosed Hospital example (on Linux or Mac):

After building the ``coroutines.jar`` file according to the [README](README.md), 
go to the ``examples`` directory, and run the bash script there:

    cd examples
    ./compilejavasim.sh

This will translate the Javasim code in the packages ``hospital`` and ``simulation``
to Java code which is placed in the new directory ``gen``.

Now, go to the ``gen`` directory, compile the Java code, and run the Hospital simulation:

    cd gen
	javac */*.java
	java hospital.Hospital

This should result in log output about patients in the following style:

    ...
	Patient P319895 arrived now hospital.Hospital@14ae5a5 at 4799994.112736705
    Patient P319895 started treatment by DR1 at 4799994.112736705
    Patient P319896 arrived now hospital.Hospital@14ae5a5 at 4799998.352456994
    Patient P319896 started treatment by DR2 at 4799998.352456994
    Patient P319895 ended treatment by DR1 at 4800000.73815024
    *** Simulation done in 4.693 seconds (real time) ***
