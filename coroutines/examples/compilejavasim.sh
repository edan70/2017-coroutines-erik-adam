#!/bin/bash

l=$(ls **/*.{java,javasim})

echo "$l"

for file in $l
do
    DIR="$(dirname $file)"
    FNAME_NO_EXT=$(basename "${file%.*}")
    TARGET_DIR="gen/$DIR"
    TARGET="$TARGET_DIR/$FNAME_NO_EXT.java"
    
    mkdir -pv gen/$DIR    # Create only if it does not already exist.

    echo "compiling: $file --> $TARGET"
    java -jar ../coroutines.jar $file > $TARGET
done
