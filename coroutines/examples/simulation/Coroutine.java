package simulation;

public interface Coroutine {

    public void doActivate();
    
    // must be visible to compiler in order to apply compiler extension.
    default void detach() {};
    
}
