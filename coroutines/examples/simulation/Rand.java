package simulation;

import java.util.Random;

public class Rand {

    private Random generator;
    private long seed;
    
    public Rand(long seed) {
	generator = new Random(seed);
	this.seed = seed;
    }

    public double rect(double least, double bound){
	double d = generator.nextDouble(); // [0.0, 1.0]
	double diff = bound - least;     
	return least + d * diff; // shift into [least, bound]
    }
    
    public double exp(double lambda){
	double u = generator.nextDouble();
	return Math.log(1-u)/(-lambda);
    }
    
    public double gauss(double mean, double stddev){
	double r = generator.nextGaussian();
	return r*stddev+mean;
    }
}
