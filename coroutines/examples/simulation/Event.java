package simulation;

public class Event {

	private double t;
	private SProcess p;
	private String eventType;

	public Event(String eventType) {
		t = 0.0;
		this.eventType = eventType;
	}

	public Event() {
		this(null);
	}


	public void setTime(double t) {
		this.t = t;
	}

	public double getT() {
		return t;
	}

	public void setProcess(SProcess p) {
		this.p = p;
	}

	public SProcess getProcess() {
		return p;
	}

	public String type() {
		return eventType;
	}

	public String toString() {
		return "E:" + t + "  ";
	}   
}
