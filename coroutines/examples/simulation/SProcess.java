package simulation;

public abstract class SProcess implements Coroutine {

	protected Simulation s;	
	protected Rand rand;

	public SProcess (Simulation s) {
		this.s = s;
		rand = new Rand(1147);
	}

	public void doActivate() {}

	// Hold(period)
	protected void hold(double duration) {
		Event e = new Event();
		e.setProcess(this);
		e.setTime(duration + s.getTime());
		s.addEvent(e);
	}

	// Activate p;
	protected void activateNow(SProcess p) {
		Event e = new Event();
		e.setProcess(p);
		e.setTime(s.getTime());
		s.addEvent(e);
	}
	// Activate p delay d;
	protected void activateDelay(SProcess p, double delay) {
		Event e = new Event();
		e.setProcess(p);
		e.setTime(s.getTime()+delay);
		s.addEvent(e);
	}
}
