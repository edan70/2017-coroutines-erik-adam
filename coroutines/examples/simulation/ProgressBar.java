package simulation;

public class ProgressBar extends SProcess {

	private static int nrMarks = 40;
	private double timeBetweenMarks;

	public ProgressBar(Simulation s, double simTime) {
		super(s);
		timeBetweenMarks = simTime/nrMarks;
		this.hold(timeBetweenMarks);
	}

	public void doActivate() {
		System.out.print("x");
		this.hold(timeBetweenMarks);
	}
}
