package hospital;

import java.util.LinkedList;
import java.util.List;

import simulation.*;

public class Hospital extends SProcess {

	public static void main(String[] args) {
		Simulation s = new Simulation(!loggingOn);
		Hospital h = new Hospital(s);
		Sampler sampler = new Sampler(s, h);
		s.simulate(8*60*10000);
		sampler.done();
	}

	public static double interArrivalTime = 15.0;
	public static double treatmentTime = 10;
	public static int nrDoctors = 2;
	public static double sampleInterval = 60;

	private static boolean loggingOn = true;  // true -> print events on std out.

	//private Simulation s;

	LinkedList<Doctor> coffeeRoom = new LinkedList<Doctor>();
	LinkedList<Patient> waitRoom = new LinkedList<Patient>();

	public Hospital(Simulation s) {
		super(s);
		this.s = s;
		for (int i = 0; i<nrDoctors; i++)
			coffeeRoom.add(new Doctor(s, this, "DR"+(i+1)));
		// start the generation of Patients
		activateNow(this);
	}

	int n; // number of patients so far
	public void doActivate() {
		// while true do
		while(true) {
			n++;
			Patient p = new Patient("P"+n);
			waitRoom.add(p);
			log("Patient " + p + " arrived now " + this);

			// Activate coffeeRoom.first
			if (!coffeeRoom.isEmpty()) {
				activateNow(coffeeRoom.removeFirst()); // "newPatient" to a doctor
			}
			//Hold(negexp(...)
			hold(rand.exp(1/Hospital.interArrivalTime));
			detach();
		}
	}

	public int nrJobs() {
		int nrDoctorsWorking = nrDoctors - coffeeRoom.size();
		return waitRoom.size() + nrDoctorsWorking;   // queue length + nr occupied servers
	}

	public void log(String text) {
	    if (loggingOn) 
		System.out.println(text + " at " + s.getTime());
	    
	}	
}
