package hospital;

import java.io.IOException;
import java.io.PrintWriter;

import simulation.*;

public class Sampler extends SProcess {	
	private PrintWriter file;
	private Hospital h;
	
	public Sampler(Simulation s, Hospital h) {
		super(s);
		this.h = h;
		try{
		    file = new PrintWriter("output.sim", "UTF-8");  // textfile
		    file.println("NrJobs");  // heading
		} catch (IOException e) {
		   System.out.println("Error opening file");
		}
		activateNow(this); // start the sampler
	}

	@Override
	public void doActivate() {
		file.println("" + h.nrJobs());
		hold(rand.exp(1/Hospital.sampleInterval));
	}
	
	public void done() {
		file.close();
	}

}
