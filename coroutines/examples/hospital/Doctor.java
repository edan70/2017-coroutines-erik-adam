package hospital;

import simulation.*;

public class Doctor extends SProcess {

	private String name;
	private Patient currentPatient;
	private Hospital h;

	public Doctor(Simulation s, Hospital h, String name) {
		super(s);
		this.name = name; 
		this.h = h;
	}

	public void doActivate() {
		while(true) {
			if (h.waitRoom.isEmpty()){
				h.coffeeRoom.add(this); // wait(coffeeRoom)
				return;
			} else {
				currentPatient = h.waitRoom.removeFirst();
				h.log("Patient " + currentPatient + " started treatment by " + this);
				hold(rand.exp(1/Hospital.treatmentTime));
				detach();
				h.log("Patient " + currentPatient + " ended treatment by " + this);
			}
		}
	}

	public String toString() {
		return name;
	}
}
