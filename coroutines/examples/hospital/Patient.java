package hospital;

public class Patient {
	
	private String name;
	
	public Patient(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}

}
