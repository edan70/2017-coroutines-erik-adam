package org.extendj;

import java.util.Arrays;
import org.extendj.ast.CompilationUnit;

public class ExtensionMain extends JavaChecker {

	public static void main(String args[]) {
		ExtensionMain emain = new ExtensionMain();
		int exitCode = emain.run(args);
		if (exitCode != 0) {
			System.exit(exitCode);
		}

		// No errors.
		/*
		   for (String s : emain.program.coroutineUseLog()) {
		   System.out.println(s);
		   }
		   */

		emain.program.prettyPrint(System.out);
	}

	@Override
	protected int processCompilationUnit(CompilationUnit unit) throws Error {
		// Replace the following super call to skip semantic error checking in unit.
		return super.processCompilationUnit(unit);
	}
}
