package org.extendj;

import static org.junit.Assert.assertEquals;
import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.io.*;
import java.nio.file.*;

import java.lang.Process;
import java.lang.ProcessBuilder;

import org.extendj.ast.*;
    
/**
 * This is a parameterized test: one test case is generated for each input
 * file found in TEST_DIRECTORY. Input files should have the ".in" extension.
 */
@RunWith(Parameterized.class)
public class TestCoroutines extends JavaChecker {
    /* Directory where the test input files are stored. */
    private static final File TEST_SOURCE_DIRECTORY = new File("testfiles");
    private static final File TEST_TARGET_DIRECTORY = new File("testfiles");
    
    private final String filename;
    public TestCoroutines(String testFile) {
	filename = testFile;
    }
    
    private String[] prepareArgs(String fname) {
	String[] argss = new String[1];
	argss[0] = fname;
	return argss;	
    }
    
    @Test public void runTest() throws Exception {	    
	int exitCode =
	    run(prepareArgs(TEST_SOURCE_DIRECTORY + "/" + filename));
	
	if (exitCode != 0) {
	    System.exit(exitCode);
	}
	
	File javaFile =
	    new File(TEST_TARGET_DIRECTORY,
		     Util.changeExtension(filename, ".java"));
	
	PrintStream out =
	    new PrintStream(new FileOutputStream(javaFile));
	program.prettyPrint(out);
	out.close();

	File testResultFile =
	    new File(TEST_TARGET_DIRECTORY,
		     Util.changeExtension(filename, ".testResult"));

	String testOutput = "";

	try {

	    String className = TEST_TARGET_DIRECTORY + "/" + Util.changeExtension(filename, "");
	    
	    ProcessBuilder compileBuilder =
			new ProcessBuilder(
					"javac",
				   	className + ".java"
					);
	    
	    
	    compileBuilder.redirectError(testResultFile);
	    compileBuilder.redirectOutput(testResultFile);
	    Process compileProcess = compileBuilder.start();
	    compileProcess.waitFor();

	    ProcessBuilder runBuilder =
			new ProcessBuilder(
					"java",
				   	className
					);
	    
	    runBuilder.redirectOutput(testResultFile);
	    runBuilder.redirectError(testResultFile);
	    Process runProcess = runBuilder.start();
	    runProcess.waitFor();

		testOutput = Util.readFileToString(testResultFile);

	    ProcessBuilder cleanBuilder = 
			new ProcessBuilder(
				   	"rm",
				   	className + ".java",
				   	"&&",
					"rm",
					className + ".class",
				   	"&&",
					"rm",
					className + ".testResult"
					);

		Process cleanProcess = cleanBuilder.start();
		cleanProcess.waitFor();
	    
	} catch(IOException e) {
	    System.err.println(e);
	    e.printStackTrace(System.err);
	}
		assertEquals("Output is not 7", "7", testOutput);
    }
    
    @Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
	return Util.getTestParameters(TEST_SOURCE_DIRECTORY,
				      ".javasim");
    }
}
