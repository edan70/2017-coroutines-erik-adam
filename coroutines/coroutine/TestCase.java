package coroutine;

public abstract class TestCase implements coroutine.Coroutine {
    private int it;
    protected int output;
    
    public TestCase(int it) {
	this.it = it;
	output = 0;
    }
    
    public void runTest() {
	while (it-- > 0) { doActivate(); }
	System.out.println(output);
    }
    
    public abstract void doActivate();
}
