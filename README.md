# ExtendJ Coroutines

This repository contains a compiler extension providing support for coroutines in Java.
The extension is based on the [Extension Base](https://bitbucket.org/extendj/extension-base)
project which sets up the basic environment needed to create a compiler extension for
[ExtendJ](https://bitbucket.org/extendj).

## Cloning this project

In order to download and build this project you will need [Git](https://git-scm.com/)
and also (optionally) [Gradle](https://gradle.org/). The
[repository](https://bitbucket.org/edan70/2017-coroutines-erik-adam) is hosted on
Atlassian BitBucket. You can clone the repository with git using the following
command:
```
git clone --recursive https://bitbucket.org/edan70/2017-coroutines-erik-adam.git
```
If the `--recursive` flag is omitted, the submodules of the project must be
initialized and updated (cloned) manually using the following commands:
```
git submodule init
git submodule update
```
This should download the ExtendJ Git repository into a local directory named
`extendj`. 

(This project also contains the ExtendJ
[Regression Tests](https://bitbucket.org/extendj/regression-tests) project
as a submodule. However, it will not be utilized until code generation is implemented.)

## Build and Run

First, enter the coroutines directory by executing the following command:
```
cd coroutines
```

The project can be built using Gradle. If you have gradle installed, the extension
is build with the command:
```
gradle jar
```
If you do not have gradle installed, you can use the *gradlew* script which
is located in the coroutine folder.

In order to run the script, ``cd`` into the *coroutine* folder and run the 
following system dependent command in a terminal:

If you are on a Unix system:
```
./gradlew
```

If you are on a Windows system:
```
gradlew
```

This will download the specific version that was used to generate the gradlew script
and build the project. Using the gradlew script to build to project is therefore
recommended.

Building the project results in a jar file named coroutines.jar which can be linked
with an application.

We refer to the [user manual](USERMANUAL.md) for examples and information on how this extension may
be used.
