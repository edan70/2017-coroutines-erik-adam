\section{Implementation}
This section describes our implementation of the coroutine extension
to ExtendJ.

%ClassDecl -> nta producing new ClassDecl --> prettyPrint the new one
%MethodDecl -> if doActivate --> traverse body and blockize/desugar.
%<-- CoStmt
%<-- genCases
%<-- switchBody
%<-- whileBody

Before going into the implementation details, lets review the goal of this
extension. The goal is to read source files containing Javasim code and
produce the equivalent standard Java source code as output. Hence,  
the compiler extension presented in this article simply provide syntactic
sugar on top of the Java language.

An important design choice made early-on is that the Javasim
language is syntactically equivalent to standard Java. If the compiler
does not recognize extension specific triggers in the input file, the
resulting output will be equal to the input.

This implementation effectively provide asymmetric coroutines as
first-class objects but not stackful. A single method,
\texttt{doActivate} is used as the global (re-)entry point for a
coroutine. Method calls in the body of \texttt{doActivate} that at
some point leads to a call to \texttt{detach} are inlined. This
effectively vanquish the requirement for a stack but at the same time
prohibit the interpretation of recursive coroutines.

There are two aspects (not entirely independent) of a coroutine
implementation which requires special attention. The first is
data and state management and the second is control flow \cite{LI04}.

The local state of a coroutine has to be stored in such a way that it does not fall 
out of scope as the coroutine  detaches. Since each coroutine is meant to be 
implemented in a separate class, the straightforward choice is to move all local 
state defined in detaching methods into its body as protected field declarations.
State includes local variable declarations as well as method  parameters.
Constructors may not detach and are therefore always copied as they are
defined by the user.

Now then, since recursion or any form of circular activation scheme is prohibited, 
uses of the parameters and local variables of a detaching methods never nest. It is
therefore safe to create a single set of parameters and local variables as field
declarations for each detaching method in each coroutine class.

In order to be able to invoke an overridden version of a detaching method, the field
declarations representing the parameters must be visible in derived classes. Since,
such methods must also be able to reference local state when inlined, local state
and parameters should be declared with protected access when transferred to the
coroutine class as field declarations.

We will now consider the method by which the coroutine body, the \texttt{doActivate}
method, is translated from Javasim into standard Java and how control flows within a
coroutine. Note that Javasim has the same semantics as standard Java, but also contains
the control statement \texttt{detach} which is realized as a method invokation to the
default method \texttt{detach} in the Coroutine interface.

A coroutine body is translated to a switch-statement wrapped in a while(true)-
statement in
standard Java. The switch-statement has a state variable as expression and can therefore
be used to access a specific case. The while statement makes sure that several cases
can be accessed during  each activation. At the end of each case, the state variable is updated
to specify the next block to be executed, followed by either a break or a return statement
to continue to the next case or to detach, respectively.

To generate a sequence of switch-cases from a coroutine body, all control constructs,
control statements, and detaching method accesses each must be dissassembled into one or more
blocks which can be collected and concatenated to form a single list of blocks for the whole body. 
A detaching method call is inlined by replacing it by the corresponding sequence
of blocks that makes up the associated method body.  Since control statements cause control
to be transferred to new switch-cases, these must be treated with just as much care as calls to 
\texttt{detach}.

In order to convert Javasim statements into blocks, we introduce the abstract \texttt{CoStmt}
which is used to represent blocks of Javasim statements. From \texttt{CoStmt}, separate
classes for control structures and control statements are derived each of which
can represent the corresponding Javasim statement as a sequence of CoStmts.
All Javasim statements that do not require any special care with regards to control flow
is represented by a derived class called \texttt{CoInst} which can represent a sequence of
Javasim statements. Thus, the whole body of \texttt{doActivate} can be represented by a
sequence of CoStmts.

For example, the following Javasim coroutine body:
\begin{lstlisting}[label=lst:IfStmt, language=Java]
void doActivate() {
    if (isTrue()) {
        detach();
        System.out.println("Hello, World!");
    }
    System.out.println("Finished!");
}
\end{lstlisting}
is converted into a \texttt{CoIf} followed by a \texttt{CoInst}. \texttt{CoIf} contains a \texttt{CoStmt}
representing the condition followed by a sequence of CoStmts representing the body of the statement. The following standard Java code is generated:
\begin{lstlisting}[label=lst:CoIf, language=Java]
void doActivate() {
    while (true) {
        switch (statevar) {
        case 0: {
            statevar = (isTrue()) ? 1 : 3;
            break;
        }
        case 1: {
            statevar = 2;
            return; // detach
        }
        case 2: {
            System.out.println("Hello, World!");
            statevar = 0;
        }
        case 3: {
                System.out.println("Finished!");
                statevar = 0;
                break;
        }}
    }
}
\end{lstlisting}

A desugaring procedure is also applied in the conversion to CoStmts. 
Desugaring makes sure that local variable declarations are converted to
variable assignments of the corresponding field declarations and that uses
of those symbols are substituted accordingly. Also, detaching method calls
are transformed into assignments of corresponding parameter field
declarations followed by a \texttt{CoStmt} representing the body of the
inlined method.

Control statements all get their own derived \texttt{CoStmt}-class and are transformed directly
into instances of these. These are \texttt{CoBreak}, \texttt{CoContinue}, \texttt{CoReturn}. Calls
to \texttt{detach} are transformed into \texttt{CoDetach}.

At the time of writing, \texttt{while}, \texttt{for}, and \texttt{if} statements (including \texttt{else if}- and
\texttt{else}-variations) are considered by the implementation along-side the control statements
mentioned above. If-statements are converted into \texttt{CoIf}, for-statements into \texttt{CoFor}, and
while-statements into \texttt{CoWhile}, respectively.

On these new node types, declared using abstract syntax, attributes are defined in the
abstract syntax tree allowing us to reason in terms of \texttt{CoStmt} instead of standard Java
statements. Basically, each \texttt{CoStmt} has an attribute which determine its state (its index in the
resulting list of \texttt{CoStmt}) which can be used as a case label in the generated switch-statement.
Combining this with another attribute which determine the next block to be executed in the sequence
(preserving the semantics of standard Java extended with the detach statement) the state variable can
be updated accordingly at the end of each block.

In order to link this tree of \texttt{CoStmts} with the abstract syntax
tree produced by the parser, a \emph{non-terminal attribute}
(nta) is placed on the AST-class ClassDecl defined by ExtendJ. The nta evaluates
to a new desugared version of type ClassDecl which is used to generate
the desugared code.

Code is generated by refining the \texttt{prettyPrint} method for ClassDecl
(defined in aspect \texttt{PrettyPrint} of Java4) to evaluate the nta placed
on ClassDecl described above and to \emph{prettyPrint} the desugared version of
the class declaration instead.

The \texttt{prettyPrint} method is used to convert an abstract syntax tree into its corresponding
source code.